/* eslint camelcase: 0 */

import React from 'react';
import PropTypes from 'prop-types';
import Grade from '../grade';
import Loading from '../loading';

import './style.css';

function createMarkup(s) {
  return { __html: s };
}

const Single = ({ error, isFetching, item }) => {
  if (isFetching) return <Loading />;
  if (error) return 'Error';
  if (!item) return null;

  const { name, image, grade, address, location, description } = item;

  const locationArr = location.split(',');
  const [lat, lon] = locationArr;
  const googleLink = `https://google.com/maps/?q=${lat},${lon}`;
  const mapLink = `https://www.google.com/maps/embed/v1/place?key=AIzaSyBWxPXR1jBEg97WE1X_zpcGZPzmDxOiqS4&q=${lat},${lon}`;

  return (
    <div className="single card">
      <img className="horchata__img" src={image} alt={name} />
      <div className="horchata__body">
        <h3>{name}</h3>
        <Grade grade={grade} />
        <div className="description">
          <div dangerouslySetInnerHTML={createMarkup(description)} />
        </div>
        <div className="address">
          <a href={googleLink} rel="noopener noreferrer" target="_blank">
            {address}
          </a>
        </div>
      </div>
      <div className="horchata__map">
        <iframe title="map" frameBorder="0" src={mapLink} allowFullScreen />
      </div>
    </div>
  );
};

Single.propTypes = {
  isFetching: PropTypes.bool,
  error: PropTypes.bool,
  item: PropTypes.shape({
    name: PropTypes.string,
    image: PropTypes.string,
    grade: PropTypes.number,
    address: PropTypes.string,
    location: PropTypes.string,
  }),
};

Single.defaultProps = {
  isFetching: false,
  error: false,
  item: undefined,
};

export default Single;
