import React from 'react';
import './style.css';

const Loading = () => (
  <div className="card signal-parent">
    <div className="signal" />
  </div>
);

export default Loading;
