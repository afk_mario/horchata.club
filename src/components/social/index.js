import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faInstagram,
  faFacebook,
  faTwitter,
  faGitlab,
} from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';

import './style.css';

const Social = ({ className }) => (
  <ul className={classNames('social', className)}>
    <li>
      <a
        href="https://instagram.com/horchataclub/"
        rel="noopener noreferrer"
        target="_blank"
      >
        <FontAwesomeIcon icon={faInstagram} />
      </a>
    </li>
    <li>
      <a
        href="https://www.facebook.com/horchataclub"
        rel="noopener noreferrer"
        target="_blank"
      >
        <FontAwesomeIcon icon={faFacebook} />
      </a>
    </li>
    <li>
      <a
        href="mailto:horchataclub@gmail.com"
        rel="noopener noreferrer"
        target="_blank"
      >
        <FontAwesomeIcon icon={faEnvelope} />
      </a>
    </li>
    <li>
      <a
        href="https://twitter.com/horchata_club"
        rel="noopener noreferrer"
        target="_blank"
      >
        <FontAwesomeIcon icon={faTwitter} />
      </a>
    </li>
    <li>
      <a
        href="https://gitlab.com/afk_mcz/horchata.club"
        rel="noopener noreferrer"
        target="_blank"
      >
        <FontAwesomeIcon icon={faGitlab} />
      </a>
    </li>
  </ul>
);

Social.propTypes = {
  className: PropTypes.string,
};

Social.defaultProps = {
  className: undefined,
};

export default Social;
