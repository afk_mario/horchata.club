/* eslint camelcase: 0 */
import React from 'react';
import PropTypes from 'prop-types';
import emoji from 'react-easy-emoji';

import { ReactComponent as Star } from '../../img/star.svg';
import './style.css';

const Grade = ({ grade }) => {
  let gradeEmoji = '🙅🏻';
  switch (grade) {
    case 0:
      gradeEmoji = '🙅';
      break;
    case 1:
      gradeEmoji = '🙎';
      break;
    case 2:
      gradeEmoji = '💁';
      break;
    default:
      gradeEmoji = '🙅';
      break;
  }

  const gradeArr = [];
  for (let i = -1; i < 2; i += 1) {
    gradeArr.push(
      <Star key={i} className={i < grade ? 'filled' : 'not__filled'} />
    );
  }

  return (
    <div className="grade">
      <ul className="stars">
        {gradeArr.map(item => (
          <li key={item.key}>{item}</li>
        ))}
      </ul>
      <span className="emoji">{emoji(gradeEmoji)}</span>
    </div>
  );
};

Grade.propTypes = {
  grade: PropTypes.number.isRequired,
};

export default Grade;
