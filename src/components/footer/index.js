import React from 'react';
import Social from '../social';

import './style.css';

export default () => (
  <footer id="footer" className="center">
    <Social />
  </footer>
);
