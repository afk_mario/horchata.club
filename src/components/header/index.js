import React from 'react';
import { NavLink } from 'react-router-dom';

import Search from '../../containers/search';
import Social from '../social';

import { ReactComponent as Logo } from '../../img/logo.svg';

import './style.css';

export default () => (
  <header id="header">
    <div className="wrapper">
      <nav>
        <ul>
          <li>
            <NavLink to="/" className="logo" activeClassName="active">
              <Logo className="logo-icon" title="Horchata Club" />
            </NavLink>
          </li>
          <li>
            <NavLink to="/about" activeClassName="active">
              About
            </NavLink>
          </li>
        </ul>
      </nav>
      <Search />
      <Social className="hide-mobile" />
    </div>
  </header>
);
