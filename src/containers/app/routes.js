import About from '../../components/about';
import NotFound from '../../components/not-found';

import HorchataList from '../horchata__list';
import HorchataSingle from '../horchata__single';

export default [
  {
    name: 'home',
    path: '/',
    component: HorchataList,
    exact: true,
  },
  {
    name: 'about',
    path: '/about',
    component: About,
    navbar: true,
    exact: true,
  },
  {
    name: 'Not Found',
    path: '/404',
    component: NotFound,
    exact: true,
  },
  {
    name: 'horchata',
    path: '/:id',
    component: HorchataSingle,
    exact: true,
  },
];
