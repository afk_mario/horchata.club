import { connect } from 'react-redux';
import List from '../components/list';
import { apiFetchIfNeeded } from '../actions/actions';

const mapStateToProps = state => {
  const { apiCalls, nameFilter } = state;
  const { isFetching, items } = apiCalls.horchata || {
    isFetching: true,
    items: [],
  };

  if (isFetching) return { isFetching, items: [] };
  if (nameFilter === '') return { isFetching, items };

  return {
    isFetching,
    items: items.filter(
      ({ name }) => name.toLowerCase().indexOf(nameFilter.toLowerCase()) > -1
    ),
  };
};

const mapDispatchToProps = dispatch => {
  dispatch(apiFetchIfNeeded('horchata'));
  return {};
};

const HorchataList = connect(
  mapStateToProps,
  mapDispatchToProps
)(List);

export default HorchataList;
